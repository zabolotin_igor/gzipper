﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace GZipper
{
    class Compressor : GZipper
    {
        public Compressor(string sourceFile, string destinationFile) : base(sourceFile, destinationFile) { }



        public override void Launch()
        {
            Console.WriteLine("Compressing");
            Thread reader = new Thread(new ThreadStart(Read));
            reader.Start();

            for (int i = 0; i < threads; i++)
            {
                doneEvents[i] = new ManualResetEvent(false);
                ThreadPool.QueueUserWorkItem(Compress, i);
            }

            Thread writer = new Thread(new ThreadStart(Write));
            writer.Start();

            WaitHandle.WaitAll(doneEvents);

            if (!cancelled)
            {
                Console.WriteLine("Compressing finished");
                success = true;
            }
        }



        public void Compress(object i)
        {
            try
            {
                while (true && !cancelled)
                {
                    MemoryBlock block = readerQueue.Remove();

                    if (block == null)
                    {
                        return;
                    }
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (GZipStream gZipStreamer = new GZipStream(memoryStream, CompressionMode.Compress))
                        {

                            gZipStreamer.Write(block.Buffer, 0, block.Buffer.Length);
                        }


                        byte[] compressedData = memoryStream.ToArray();
                        MemoryBlock outData = new MemoryBlock(block.Id, compressedData);
                        writerQueue.InsertForWriting(outData);
                    }
                    ManualResetEvent doneEvent = doneEvents[(int)i];
                    doneEvent.Set();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in thread number {i}. Error description: {ex.Message}");
                cancelled = true;
            }

        }

        public override void Read()
        {
            try
            {
                using (FileStream file = new FileStream(sourceFile, FileMode.Open))
                {
                    int readBytes;
                    byte[] buffer;

                    while (file.Position < file.Length && !cancelled)
                    {
                        if (file.Length - file.Position <= blockSize)
                        {
                            readBytes = (int)(file.Length - file.Position);
                        }

                        else
                        {
                            readBytes = blockSize;
                        }
                        buffer = new byte[readBytes];
                        file.Read(buffer, 0, readBytes);
                        readerQueue.InsertForCompressing(buffer);
                    }
                    readerQueue.StopThread();
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);

                cancelled = true;
            }
        }

        public override void Write()
        {
            try
            {
                using (FileStream file = new FileStream(destinationFile + ".gz", FileMode.Append))
                {
                    while (true && !cancelled)
                    {
                        MemoryBlock block = writerQueue.Remove();
                        if (block == null)
                        {
                            return;
                        }

                        BitConverter.GetBytes(block.Buffer.Length).CopyTo(block.Buffer, 4);
                        file.Write(block.Buffer, 0, block.Buffer.Length);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                cancelled = true;
            }

        }
    }
}
