﻿using System;
using System.Threading;

namespace GZipper
{
    abstract class GZipper
    {
        protected static int threads = Environment.ProcessorCount;

        protected string sourceFile;

        protected string destinationFile;

        protected int blockSize = 1024 * 1024;

        protected bool cancelled = false;

        protected bool success = false;

        protected MemoryQueue readerQueue = new MemoryQueue();

        protected MemoryQueue writerQueue = new MemoryQueue();

        protected ManualResetEvent[] doneEvents = new ManualResetEvent[threads];

        public GZipper(string sourceFile, string destinationFile)
        {
            this.sourceFile = sourceFile;
            this.destinationFile = destinationFile;
        }

        public int Result()
        {
            if (!cancelled && success)
            {
                return 0;
            }
            return 1;
        }

        public void Cancel()
        {
            Console.WriteLine("Canselling");
            cancelled = true;
        }

        public abstract void Launch();

        public abstract void Read();

        public abstract void Write();

    }
}
