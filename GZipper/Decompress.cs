﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;

namespace GZipper
{
    class Decompress : GZipper
    {
        private int counter = 0;
        public Decompress(string sourceFile, string destinationFile) : base(sourceFile, destinationFile) { }


        public override void Launch()
        {


            Thread reader = new Thread(new ThreadStart(Read));
            reader.Start();

            for (int i = 0; i < threads; i++)
            {
                doneEvents[i] = new ManualResetEvent(false);
                ThreadPool.QueueUserWorkItem(Decompressing, i);
            }

            Thread _writer = new Thread(new ThreadStart(Write));
            _writer.Start();

            WaitHandle.WaitAll(doneEvents);

            if (!cancelled)
            {
                Console.WriteLine("Decompressing finished");
                success = true;
            }
        }

        public override void Read()
        {
            try
            {
                using (FileStream compressedFile = new FileStream(sourceFile, FileMode.Open))
                {
                    while (compressedFile.Position < compressedFile.Length)
                    {
                        byte[] lengthBuffer = new byte[8];
                        compressedFile.Read(lengthBuffer, 0, lengthBuffer.Length);
                        int blockLength = BitConverter.ToInt32(lengthBuffer, 4);
                        byte[] compressedData = new byte[blockLength];
                        lengthBuffer.CopyTo(compressedData, 0);

                        compressedFile.Read(compressedData, 8, blockLength - 8);
                        int dataSize = BitConverter.ToInt32(compressedData, blockLength - 4);
                        byte[] lastBuffer = new byte[dataSize];

                        MemoryBlock block = new MemoryBlock(counter, lastBuffer, compressedData);
                        readerQueue.InsertForWriting(block);
                        counter++;

                    }
                    readerQueue.StopThread();
                }
            }
            catch (Exception ex)
            {
                cancelled = true;
                Console.WriteLine(ex.Message);
            }
        }

        public override void Write()
        {
            try
            {
                using (FileStream decompressedFile = new FileStream(sourceFile.Remove(sourceFile.Length - 3), FileMode.Append))
                {
                    while (true && !cancelled)
                    {
                        MemoryBlock block = writerQueue.Remove();
                        if (block == null)
                        {
                            return;
                        }

                        decompressedFile.Write(block.Buffer, 0, block.Buffer.Length);
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                cancelled = true;
            }
        }

        public void Decompressing(object i)
        {
            try
            {
                while (true && !cancelled)
                {
                    MemoryBlock block = readerQueue.Remove();
                    if (block == null)
                    {
                        return;
                    }

                    using (MemoryStream memoryStream = new MemoryStream(block.CompressedBuffer))
                    {
                        using (GZipStream gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                        {
                            gZipStream.Read(block.Buffer, 0, block.Buffer.Length);
                            byte[] decompressedData = block.Buffer.ToArray();
                            MemoryBlock _block = new MemoryBlock(block.Id, decompressedData);
                            writerQueue.InsertForWriting(_block);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine($"Error in thread number {i}.Error description: {ex.Message}");
                cancelled = true;
            }
        }
    }
}
