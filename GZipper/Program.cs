﻿using System;
using System.IO;

namespace GZipper
{
    class Program
    {
        static GZipper zip;
  
        static int Main(string[] args)
        {
            Console.WriteLine("To compress file or decompress please Go to GZipper.exe derictory file and enter : \n " +
              "GZipper.exe compress/decompress [source file path] [destination file path]");

            Console.CancelKeyPress += new ConsoleCancelEventHandler(CanselPressed);

            try
            {
                ReadArgs(args);

                switch (args[0].ToLower())
                {
                    case "compress":
                        zip = new Compressor(args[1], args[2]);
                        break;
                    case "decompress":
                        zip = new Decompress(args[1], args[2]);
                        break;
                }

                zip.Launch();
                return zip.Result();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error !\n Method: {ex.TargetSite}\n Error description {ex.Message}");
                return 1;
                
            }
        }


        public static void ReadArgs(string[] args)
        {
            if (args[0].ToLower() != "compress" && args[0].ToLower() != "decompress")
            {
                throw new Exception("First argument must be *compress or *decompress");
            }

            if (args[1].Length == 0)
            {
                throw new Exception("No source file name");
            }

            if (!File.Exists(args[1]))
            {
                throw new Exception("No source file was found.");
            }

            FileInfo fileIn = new FileInfo(args[1]);
            FileInfo fileOut = new FileInfo(args[2]);

            if (args[1] == args[2])
            {
                throw new Exception("Source and destination files shall be different.");
            }

            if (fileIn.Extension == ".gz" && args[0] == "compress")
            {
                throw new Exception("File has already been compressed.");
            }

            if (fileOut.Extension == ".gz" && fileOut.Exists)
            {
                throw new Exception("Destination file already exists. Please indiciate the different file name.");
            }

            if (fileIn.Extension != ".gz" && args[0] == "decompress")
            {
                throw new Exception("File to be decompressed shall have .gz extension.");
            }

            if (args[2].Length == 0)
            {
                throw new Exception("No destination file name was specified.");
            }
        }

        static void CanselPressed(object sender, ConsoleCancelEventArgs _args)
        {
            zip.Cancel();
        }
    }
}
