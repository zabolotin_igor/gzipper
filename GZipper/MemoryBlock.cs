﻿namespace GZipper
{
    public class MemoryBlock
    {
        private int id;

        private byte[] buffer;

        private byte[] compressedBuffer;

        public int Id { get { return id; } }

        public byte[] Buffer { get { return buffer; } }

        public byte[] CompressedBuffer { get { return compressedBuffer; } }


        public MemoryBlock(int id,byte[] buffer) : this (id,buffer,new byte[0]){ }

        public MemoryBlock(int id,byte[] buffer, byte[] compressedBuffer)
        {
            this.id = id;

            this.buffer = buffer;

            this.compressedBuffer = compressedBuffer;
        }

    }
}
