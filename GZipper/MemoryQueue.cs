﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace GZipper
{
    class MemoryQueue
    {
        private object locker = new object();

        Queue<MemoryBlock> queue = new Queue<MemoryBlock>();

        private bool isAlive = true;

        private int memoryBlockId = 0;

        public void StopThread()
        {
            lock (locker)
            {
                isAlive = false;
                Monitor.PulseAll(locker);
            }
        }

        public void InsertForWriting(MemoryBlock block)
        {
            int id = block.Id;

            lock (locker)
            {
                if (!isAlive)
                {
                    throw new Exception("Thread is already stopped");
                }

                while (id != memoryBlockId)
                {
                    Monitor.Wait(locker);
                }

                queue.Enqueue(block);
                memoryBlockId++;
                Monitor.PulseAll(locker);
            }
        }

        public void InsertForCompressing(byte[] block)
        {
            lock (locker)
            {
                if (!isAlive)
                {
                    throw new Exception("Thread is already stopped");
                }

                MemoryBlock memoryBlock = new MemoryBlock(memoryBlockId, block);
                queue.Enqueue(memoryBlock);
                memoryBlockId++;
                Monitor.PulseAll(locker);
            }
        }
        
        public MemoryBlock Remove()
        {
            lock (locker)
            {
                while(queue.Count==0 && isAlive)
                {
                    Monitor.Wait(locker);
                }

                if (queue.Count == 0)
                {
                    return null;
                }
                return queue.Dequeue();
            }
        }

    }
}
